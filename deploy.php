<?php
require 'recipe/common.php';

task('deploy', [
	'deploy:prepare',
	'deploy:release',
	'deploy:update_code',
	'deploy:shared',
	'deploy:writable',
	'deploy:vendors',
	'deploy:symlink',
	'cleanup'
])->desc('Deploy your project');

localServer('main', 'dev.remoteo.com')
	->env('deploy_path', '/var/www/dev.remoteo.com');

set('repository', 'git@bitbucket.org:CoolLamer/coworking.git');
env('branch', 'dev');
set('shared_dirs', ['log']);
set('shared_files', ['app/config/config.local.neon']);
set('writable_dirs', ['temp', 'log', 'www/webtemp']);
set('writable_use_sudo', false); // Using sudo in writable commands?
get('keep_releases', 3);
