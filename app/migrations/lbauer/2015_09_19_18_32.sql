CREATE TABLE contact (
  id             INT AUTO_INCREMENT NOT NULL,
  email          VARCHAR(255)       NOT NULL,
  ip_address     VARCHAR(255)       NOT NULL,
  user_agent     VARCHAR(255)       NOT NULL,
  created_at     DATETIME           NOT NULL,
  facebook_token VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
CREATE TABLE contact_question (
  id                  INT AUTO_INCREMENT NOT NULL,
  question_text       VARCHAR(255)       NOT NULL,
  question_definition VARCHAR(255)       NOT NULL,
  created_at          DATETIME           NOT NULL,
  language            VARCHAR(255)       NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
CREATE TABLE contact_question_answer (
  id          INT AUTO_INCREMENT NOT NULL,
  question_id INT DEFAULT NULL,
  contact_id  INT DEFAULT NULL,
  answer      VARCHAR(255)       NOT NULL,
  created_at  DATETIME           NOT NULL,
  INDEX IDX_46975A131E27F6BF (question_id),
  INDEX IDX_46975A13E7A1254A (contact_id),
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
CREATE TABLE user (
  id        INT AUTO_INCREMENT NOT NULL,
  user_name VARCHAR(255)       NOT NULL,
  level     VARCHAR(255)       NOT NULL,
  password  VARCHAR(255)       NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
ALTER TABLE contact_question_answer ADD CONSTRAINT FK_46975A131E27F6BF FOREIGN KEY (question_id) REFERENCES contact_question (id);
ALTER TABLE contact_question_answer ADD CONSTRAINT FK_46975A13E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id);