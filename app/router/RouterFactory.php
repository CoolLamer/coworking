<?php
namespace Remoteo;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

class RouterFactory
{
	/**
	 * @return \Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$rootRouter = new RouteList();
		$rootRouter[] = self::createAdminRouter();
		$rootRouter[] = self::createFrontRouter();
		return $rootRouter;
	}

	/**
	 * @return RouteList
	 */
	private static function createFrontRouter()
	{
		$frontRouter = new RouteList('Front');
		$frontRouter[] = new Route('[<locale=en cs|en>/]<presenter>/<action>[/<id>]', 'Homepage:default');
		return $frontRouter;
	}

	private static function createAdminRouter()
	{
		$frontRouter = new RouteList('Admin');
		$frontRouter[] = new Route('admin/<presenter>/<action>[/<id>]', 'Homepage:default');
		return $frontRouter;
	}
}
