<?php
namespace Remoteo\Modules\AdminModule\Presenters;

use Mesour\DataGrid\DoctrineDataSource;
use Mesour\DataGrid\Grid;
use Remoteo\Contact\ContactService;

class HomepagePresenter extends AdminPresenter
{
	/**
	 * @inject
	 * @var ContactService
	 */
	public $contactService;

	protected function createComponentContactGrid()
	{
		$source = new DoctrineDataSource($this->contactService->getAdminTableQuery());
		$grid = new Grid();
		$grid->addText('email', 'Email');
		$grid->setDataSource($source);
		$grid->enablePager();
		return $grid;
	}
}