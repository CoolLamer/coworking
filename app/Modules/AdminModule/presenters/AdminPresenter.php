<?php
namespace Remoteo\Modules\AdminModule\Presenters;


use Remoteo\Modules\CoreModule\Presenters\BasePresenter;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;

abstract class AdminPresenter extends BasePresenter
{
	public function startup(){
		parent::startup();
		if(!$this->user->isLoggedIn()){
			$this->flashMessage('Je potřeba se přihlásit');
			$this->toLoginPage();
		}
		if(!$this->user->isInRole('admin')){
			$this->flashMessage('Sem nemáš přístup');
			$this->toDefaultPage();
		}
	}

	public function toLoginPage()
	{
		$this->redirect(':Admin:Sign:in');
	}

	/** @return CssLoader */
	protected function createComponentCss()
	{
		return $this->webLoader->createCssLoader('admin');
	}

	/** @return JavaScriptLoader */
	protected function createComponentJs()
	{
		return $this->webLoader->createJavaScriptLoader('admin');
	}
}