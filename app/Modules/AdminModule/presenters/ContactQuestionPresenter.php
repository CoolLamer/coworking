<?php
namespace Remoteo\Modules\AdminModule\Presenters;

use Remoteo\Modules\AdminModule\ContactQuestion\IContactQuestionControl;

class ContactQuestionPresenter extends AdminPresenter
{
	public function actionCreate()
	{
		$this->setView('edit');
	}

	public function actionEdit($id)
	{
		$this['form']->setId($id);
	}

	public function createComponentForm(IContactQuestionControl $contactQuestionControl)
	{
		return $contactQuestionControl->create();
	}
}