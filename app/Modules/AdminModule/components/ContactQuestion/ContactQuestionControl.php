<?php
namespace Remoteo\Modules\AdminModule\ContactQuestion;

use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Remoteo\Contact\ContactQuestionRepository;
use Remoteo\Modules\CoreModule\Components\BaseFormControl;
use Remoteo\Modules\CoreModule\Components\IBaseForm;

class ContactQuestionControl extends BaseFormControl
{
	/**
	 * @var ContactQuestionRepository
	 */
	private $contactQuestionService;

	/**
	 * @var int
	 */
	private $id = null;

	public function __construct(ContactQuestionRepository $contactQuestionService)
	{
		$this->contactQuestionService = $contactQuestionService;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function createComponentForm(IBaseForm $formFactory)
	{
		$form = $formFactory->create();
		$form->addText('questionText', 'Text otázky');
		$form->addTextArea('questionDefinition', 'Definice otázky');
		$form->addSelect('language', 'Jazyk', $this->getLanguages());
		$form->addSubmit('submit');
		$this->setDefaultValues($form);
		$form->onSuccess[] = [$this, 'processForm'];
		return $form;
	}

	public function processForm(Form $form)
	{
		$values = $form->getValues(true);
		if ($this->id == null) {
			$id = $this->contactQuestionService->create($values);
			$this->getPresenter()->redirect('edit', $id);
		} else {
			$this->contactQuestionService->update($this->id, $values);
			$this->getPresenter()->redirect('this');
		}
	}

	private function setDefaultValues(Form $form)
	{
		if ($this->id != null) {
			$contactQuestion = $this->contactQuestionService->get($this->id);
			if (!$contactQuestion) {
				throw new BadRequestException();
			}
			$form->setDefaults($contactQuestion->toArray());
		}
	}

	private function getLanguages()
	{
		return [
			'cs' => 'Česky',
			'en' => 'Anglicky'
		];
	}
}