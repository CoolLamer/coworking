<?php
namespace Remoteo\Modules\AdminModule\ContactQuestion;


interface IContactQuestionControl
{
	/**
	 * @return ContactQuestionControl
	 */
	public function create();
}