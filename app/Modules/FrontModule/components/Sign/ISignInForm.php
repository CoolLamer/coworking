<?php
namespace Remoteo\Modules\FrontModule\Components\Sign;

interface ISignInForm
{
    /**
     * @return SignInForm
     */
    public function create();
} 