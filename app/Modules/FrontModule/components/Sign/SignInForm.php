<?php
namespace Remoteo\Modules\FrontModule\Components\Sign;

use Nette\Security\AuthenticationException;
use Remoteo\Modules\CoreModule\Components\BaseForm;
use Remoteo\Modules\CoreModule\Components\BaseFormControl;
use Remoteo\Modules\CoreModule\Components\IBaseForm;
use Remoteo\Modules\CoreModule\Presenters\BasePresenter;

class SignInForm extends BaseFormControl
{
	public function createComponentForm(IBaseForm $form)
	{
		$form = $form->create();
		$form->addText('username', 'Username:')
			->setRequired('messages.user.enterUsername');
		$form->addPassword('password', 'Password:')
			->setRequired('messages.user.enterPassword');
		$form->addSubmit('send', 'messages.user.loginButton');
		$form->onSuccess[] = [$this, 'onSuccess'];
		return $form;
	}

	public function onSuccess(BaseForm $form)
	{
		$values = $form->getValues();
		$this->presenter->getUser()->setExpiration('6 hours', true);

		try {
			/** @var BasePresenter $presenter */
			$presenter = $this->getPresenter();
			$presenter->getUser()->login($values->username, $values->password);
			$presenter->toDefaultPage();
		} catch (AuthenticationException $e) {
			$this->flashMessage($e->getMessage(), 'warning');
		}
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/signInForm.latte');
		$this->template->render();
	}
} 