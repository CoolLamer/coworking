<?php
namespace Remoteo\Modules\FrontModule\ContactQuestion;

interface IContactQuestionControl
{
	/**
	 * @return ContactQuestionControl
	 */
	public function create();
}