<?php
namespace Remoteo\Modules\FrontModule\ContactQuestion;

use Nette\Application\UI\Form;
use Remoteo\Contact\ContactQuestionRepository;
use Remoteo\Contact\ContactQuestion;
use Remoteo\Modules\CoreModule\Components\BaseControl;
use Remoteo\Modules\CoreModule\Components\IBaseForm;

class ContactQuestionControl extends BaseControl
{
	/**
	 * @persistent
	 * @var int
	 */
	public $questionId;
	/**
	 * @var ContactQuestionRepository
	 */
	private $contactQuestionRepository;
	/**
	 * @var array
	 */
	private $answers;
	/**
	 * @var ContactQuestion
	 */
	public $question;

	public function __construct(ContactQuestionRepository $contactQuestionService)
	{
		$this->contactQuestionRepository = $contactQuestionService;
	}

	public function createComponentForm(IBaseForm $formFactory)
	{
		$this->prepareQuestion();
		$form = $formFactory->create();
		$form->addHidden('questionId')->setValue($this->questionId);
		$form->addRadioList('answer', null, $this->answers);
		$form->addSubmit('send');
		$form->onSuccess[] = [$this, 'processForm'];
		return $form;
	}

	public function processForm(Form $form)
	{
		$values = $form->getValues();
		$userId = $this->getPresenter()->getUser()->getId();
		$this->contactQuestionRepository->createAnswer($this->question, $userId, $values->answer);
		$this->redirect('this', ['questionId' => null]);
	}

	public function render()
	{
		$this->prepareQuestion();
		if ($this->question) {
			$this->template->setFile(__DIR__ . '/contactQuestionControl.latte');
			$this->template->question = $this->question->getQuestionText();
		} else {
			$this->template->setFile(__DIR__ . '/contactQuestionControl.noQuestion.latte');
		}
		$this->template->render();
	}

	private function setAnswersFromString($answersString)
	{
		foreach (explode("\n", $answersString) as $row) {
			$data = explode('|', $row);
			$this->answers[$data[0]] = $data[1];
		}
	}

	private function prepareQuestion()
	{
		if ($this->question == null) {
			if ($this->questionId == null) {
				$this->question = $this->contactQuestionRepository->getQuestionForUser($this->getPresenter()->getUser()->getId());
			} else {
				$this->question = $this->contactQuestionRepository->get($this->questionId);
			}
			if ($this->question) {
				$this->questionId = $this->question->getId();
				$this->setAnswersFromString($this->question->getQuestionDefinition());
			}
		}
	}
}