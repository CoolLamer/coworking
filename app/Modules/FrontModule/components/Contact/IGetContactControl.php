<?php
namespace Remoteo\Modules\FrontModule\Components\Contact;

interface IGetContactControl
{
	/**
	 * @return GetContactControl
	 */
	public function create();
}