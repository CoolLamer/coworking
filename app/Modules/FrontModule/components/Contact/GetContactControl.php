<?php
namespace Remoteo\Modules\FrontModule\Components\Contact;

use Nette\Application\UI\Form;
use Remoteo\Modules\CoreModule\Components\BaseControl;
use Remoteo\Modules\CoreModule\Components\IBaseForm;
use Remoteo\Contact\ContactService;
use Remoteo\Services\FacebookService;

class GetContactControl extends BaseControl
{
	/**
	 * @var ContactService
	 */
	private $contactService;

	/**
	 * @var FacebookService
	 */
	public $facebookService;

	public function __construct(ContactService $contactService, FacebookService $facebookService)
	{
		$this->contactService = $contactService;
		$this->facebookService = $facebookService;
	}

	public function createComponentForm(IBaseForm $formFactory)
	{
		$form = $formFactory->create();
		$form->addText('email')
			->addRule(Form::EMAIL, 'Please enter valid email')
			->setAttribute("placeholder", "E-mail");
		$form->addSubmit('submit', "Chci volnost");
		$form->onSuccess[] = [$this, 'processForm'];
		return $form;
	}

	public function processForm(Form $form)
	{
		$values = $form->getValues();
		$identity = $this->contactService->createContact($values->email);
		$this->getPresenter()->getUser()->setExpiration('90 days', false);
		$this->getPresenter()->getUser()->login($identity);
		$this->createGaEvent();
		$this->redirect('this');
	}

	public function handleRequestFacebookLogin()
	{
		$url = $this->facebookService->requestAuthorization($this->link('//login!'));
		if ($url !== null) {
			$this->getPresenter()->redirectUrl($url);
		}
	}

	public function handleLogin()
	{
		$accessToken = $this->facebookService->processAuthorization($this->link('//login!'));
		$identity = $this->contactService->createContactFromFacebook($accessToken);
		$this->getPresenter()->getUser()->setExpiration('90 days', false);
		$this->getPresenter()->user->login($identity);
		$this->createGaEvent(true);
		$this->redirect('this');
	}

	private function createGaEvent($facebook = false)
	{
		$ssga = new \ssga('UA-67504183-1', 'remoteo.com');
		if ($facebook) {
			$ssga->set_event('contact', 'facebook');
		} else {
			$ssga->set_event('contact', 'email');
		}
		$ssga->send();
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/getContact.latte');
		$this->template->render();
	}
}