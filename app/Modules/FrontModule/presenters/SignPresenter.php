<?php
namespace Remoteo\Modules\FrontModule\Presenters;

use Kdyby\Autowired\AutowireComponentFactories;
use Remoteo\Modules\CoreModule\Presenters\BasePresenter;
use Remoteo\Modules\FrontModule\Components\Sign\ISignInForm;

class SignPresenter extends BasePresenter
{
	use AutowireComponentFactories;

	protected function createComponentSignInForm(ISignInForm $form)
	{
		$control = $form->create();
		return $control;
	}

	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('messages.user.logged-off');
		$this->redirect('in');
	}
}