<?php
namespace Remoteo\Modules\FrontModule\Presenters;

use Remoteo\Modules\CoreModule\Presenters\BasePresenter;
use Remoteo\Modules\FrontModule\Components\Contact\IGetContactControl;
use Remoteo\Modules\FrontModule\ContactQuestion\IContactQuestionControl;

class HomepagePresenter extends BasePresenter
{
	public function createComponentContactForm(IGetContactControl $factory)
	{
		return $factory->create();
	}

	public function createComponentContactQuestion(IContactQuestionControl $controlFactory)
	{
		return $controlFactory->create();
	}
}
