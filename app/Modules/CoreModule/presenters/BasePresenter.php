<?php
namespace Remoteo\Modules\CoreModule\Presenters;

use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette;
use WebLoader\LoaderFactory;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	use AutowireProperties;
	use AutowireComponentFactories;

	/** @persistent */
	public $locale;

	/**
	 * @autowire
	 * @var \Kdyby\Translation\Translator
	 */
	protected $translator;

	/**
	 * @autowire
	 * @var \Doctrine\ORM\EntityManager
	 */
	protected $entityManager;

	/** @var LoaderFactory @inject */
	public $webLoader;

	/**
	 * @doc Někde okopírovaný kód který zajištuje že do metody action* doplní entitu podle id, pokud to požaduje
	 * @param $method
	 * @param array $params
	 * @return bool
	 * @throws Nette\Application\BadRequestException
	 * @throws Nette\Application\ForbiddenRequestException
	 * @throws \Exception
	 * @throws \ReflectionException
	 */
	protected function tryCall($method, array $params)
	{
		$rc = $this->getReflection();
		if ($rc->hasMethod($method)) {
			$rm = $rc->getMethod($method);
			if ($rm->isPublic() && !$rm->isAbstract() && !$rm->isStatic()) {
				$this->checkRequirements($rm);
				$args = $rc->combineArgs($rm, $params);

				if (\Nette\Utils\Strings::match($method, "~^(action|render|handle).+~")) {

					$methodParams = $rm->getParameters();
					foreach ($methodParams as $i => $param) {
						/** @var \Nette\Reflection\Parameter $param */
						if ($className = $param->getClassName()) {
							$paramName = $param->getName();

							if ($paramValue = $args[$i]) {
								$entity = $this->findById($className, $paramValue);
								if ($entity) {
									$args[$i] = $entity;
								} else {
									$args[$i] = NULL;
								}
							} else {
								if (!$param->allowsNull()) {
									throw new \Nette\Application\BadRequestException("Value '$param' cannot be NULL.");
								}
							}

						}
					}

				}

				$rm->invokeArgs($this, $args);
				return TRUE;
			}
		}

		return FALSE;
	}

	/**
	 * Find entity by ID.
	 * @param string $entityName
	 * @param int $id
	 * @return object|null
	 */
	protected function findById($entityName, $id)
	{
		return $this->entityManager->find($entityName, $id);
	}

	public function toDefaultPage()
	{
		if ($this->user->isInRole('admin')) {
			$this->redirect(':Admin:Homepage:default');
		}
		$this->redirect(':Front:Homepage:default');
	}

	public function toLoginPage()
	{
		$this->redirect(':Front:Sign:in');
	}

	public function flashMessage($message, $type = 'info')
	{
		return parent::flashMessage($this->translator->translate($message), $type);
	}

	/** @return CssLoader */
	protected function createComponentCss()
	{
		return $this->webLoader->createCssLoader('default');
	}

	/** @return JavaScriptLoader */
	protected function createComponentJs()
	{
		return $this->webLoader->createJavaScriptLoader('default');
	}
}
