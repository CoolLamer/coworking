<?php
namespace Remoteo\Modules\CoreModule\Components;

use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;
use Nextras\Forms\Rendering\Bs3FormRenderer;

class BaseForm extends Form
{
    public function __construct(Translator $translator)
    {
        parent::__construct();
        $this->setRenderer(new Bs3FormRenderer());
        $this->setTranslator($translator);
    }
} 