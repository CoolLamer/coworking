<?php
namespace Remoteo\Modules\CoreModule\Components;

use Kdyby\Autowired\AutowireComponentFactories;
use Nette\Application\UI\Control;

class BaseControl extends Control
{
    use AutowireComponentFactories;

    public function flashMessage($message, $type = 'info')
    {
        $this->getPresenter()->flashMessage($message, $type);
    }
} 