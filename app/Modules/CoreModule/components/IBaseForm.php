<?php
namespace Remoteo\Modules\CoreModule\Components;

interface IBaseForm
{
    /**
     * @return BaseForm
     */
    public function create();
} 