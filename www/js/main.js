var windowHeight = $(window).height();
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    halfScreen = (windowHeight/2)-200;

    if (scroll >= halfScreen) {
        $(".landing .arrow").fadeOut("slow");
    } else {
        $(".landing .arrow").fadeIn("slow")
    }

});
$(document).ready(function(){
    $(".arrow-wrapper").click(function(){
         $('html, body').animate({
            'scrollTop': windowHeight
        }, 1500);
    });
    $('nav a').click(function(){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 1500);
        return false;
    });
});

/*
    POPUPS AND MODALS 
*/

$(document).ready(function () {

    var $contactModal = $('.open-popup-link');
    var $cardPopup = $('.open-card-popup');

    $contactModal.magnificPopup({
        type: 'inline',
        modal: true
    });

    $cardPopup.click(function () {

        var content = $(this).data('content');

        $.magnificPopup.open({
            items: {
                src: '<div class="service-popup"><div class="content">' + content + '</div></div>', // can be a HTML string, jQuery object, or CSS selector
                type: 'inline'
            }
        });

    });

    $('.tile a').click(function () {
        var tag = $(this).data('tag');
        //ga('send', 'event', 'link', 'click', tag);
        $contactModal.magnificPopup('open');
        return false;
    });
});
