<?php
namespace Remoteo\Services;

use Facebook\Facebook;
use Nette\Object;

class FacebookService extends Object
{
	private function getFacebook()
	{
		$fb = new Facebook([
			'app_id' => '1637141793228228',
			'app_secret' => '957b0d003443a1485d303c8747e63140',
			'default_graph_version' => 'v2.0',
		]);
		return $fb;
	}

	function requestAuthorization($callbackUrl)
	{
		$fb = $this->getFacebook();
		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email', 'user_likes']; // optional
		$loginUrl = $helper->getLoginUrl($callbackUrl, $permissions);
		return $loginUrl;
	}

	function processAuthorization()
	{
		$fb = $this->getFacebook();
		return $fb->getRedirectLoginHelper()->getAccessToken();
	}

	public function loadContactData($accessToken)
	{
		$fb = $this->getFacebook();
		$result = $fb->get('/me?fields=id,name,email', $accessToken);
		return $result->getDecodedBody();
	}
}