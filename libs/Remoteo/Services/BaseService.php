<?php
namespace Remoteo\Services;

use Kdyby\Doctrine\EntityManager;
use Nette\Object;

class BaseService extends Object
{
	/**
	 * @var EntityManager
	 */
	protected $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}
}