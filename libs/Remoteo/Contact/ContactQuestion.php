<?php
namespace Remoteo\Contact;

use Doctrine\ORM\Mapping as ORM;
use Remoteo\Entities\BaseEntity;

/**
 * @ORM\Entity
 */
class ContactQuestion extends BaseEntity
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $questionText;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $questionDefinition;

	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $createdAt;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $language;

	/**
	 * @ORM\OneToMany(targetEntity="ContactQuestionAnswer", mappedBy="question")
	 * @var ContactQuestionAnswer
	 */
	protected $answers;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getQuestionText()
	{
		return $this->questionText;
	}

	/**
	 * @param string $questionText
	 */
	public function setQuestionText($questionText)
	{
		$this->questionText = $questionText;
	}

	/**
	 * @return mixed
	 */
	public function getQuestionDefinition()
	{
		return $this->questionDefinition;
	}

	/**
	 * @param mixed $questionDefinition
	 */
	public function setQuestionDefinition($questionDefinition)
	{
		$this->questionDefinition = $questionDefinition;
	}

	/**
	 * @return mixed
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * @param mixed $createdAt
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
	}

	/**
	 * @return mixed
	 */
	public function getLanguage()
	{
		return $this->language;
	}

	/**
	 * @param mixed $language
	 */
	public function setLanguage($language)
	{
		$this->language = $language;
	}
}