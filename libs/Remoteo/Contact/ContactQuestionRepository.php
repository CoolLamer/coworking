<?php
namespace Remoteo\Contact;

use Kdyby\Doctrine\EntityManager;
use Remoteo\Services\BaseService;

class ContactQuestionRepository extends BaseService
{
	/**
	 * @var ContactService
	 */
	private $contactService;

	public function __construct(EntityManager $entityManager, ContactService $contactService)
	{
		parent::__construct($entityManager);
		$this->contactService = $contactService;
	}

	public function create($data)
	{
		$contactQuestion = new ContactQuestion();
		$contactQuestion->fromArray($data);
		$contactQuestion->setCreatedAt(new \DateTime());
		$this->entityManager->persist($contactQuestion);
		$this->entityManager->flush();
		return $contactQuestion->getId();
	}

	/**
	 * @param $id
	 * @return null|ContactQuestion
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 * @throws \Doctrine\ORM\TransactionRequiredException
	 */
	public function get($id)
	{
		return $this->entityManager->find(ContactQuestion::getClassName(), $id);
	}

	public function update($id, $data)
	{
		$contactQuestion = $this->get($id);
		$contactQuestion->fromArray($data);
		$this->entityManager->flush();
	}

	/**
	 * @return ContactQuestion
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function getQuestionForUser($contactId)
	{
		return $this->entityManager->createQueryBuilder()
			->select('q')
			->from(ContactQuestion::getClassName(), 'q')
			->leftJoin('q.answers', 'a')
			->where('a.id IS NULL AND')
			->getQuery()
			->setMaxResults(1)
			->getOneOrNullResult();
	}

	public function createAnswer(ContactQuestion $question, $userId, $answer)
	{
		$answerEntity = new ContactQuestionAnswer();
		$answerEntity->setContact($this->contactService->get($userId));
		$answerEntity->setCreatedAt(new \DateTime());
		$answerEntity->setAnswer($answer);
		$answerEntity->setQuestion($question);
		$this->entityManager->persist($answerEntity);
		$this->entityManager->flush();
	}
}