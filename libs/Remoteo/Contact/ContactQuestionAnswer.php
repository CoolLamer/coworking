<?php
namespace Remoteo\Contact;

use Doctrine\ORM\Mapping as ORM;
use Remoteo\Entities\BaseEntity;

/**
 * @ORM\Entity
 */
class ContactQuestionAnswer extends BaseEntity
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="ContactQuestion", inversedBy="answers")
	 * @var ContactQuestion
	 */
	protected $question;

	/**
	 * @ORM\ManyToOne(targetEntity="Contact")
	 * @var Contact
	 */
	protected $contact;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $answer;

	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $createdAt;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return ContactQuestion
	 */
	public function getQuestion()
	{
		return $this->question;
	}

	/**
	 * @param ContactQuestion $question
	 */
	public function setQuestion($question)
	{
		$this->question = $question;
	}

	/**
	 * @return Contact
	 */
	public function getContact()
	{
		return $this->contact;
	}

	/**
	 * @param Contact $contact
	 */
	public function setContact(Contact $contact)
	{
		$this->contact = $contact;
	}

	/**
	 * @return mixed
	 */
	public function getAnswer()
	{
		return $this->answer;
	}

	/**
	 * @param mixed $answer
	 */
	public function setAnswer($answer)
	{
		$this->answer = $answer;
	}

	/**
	 * @return mixed
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * @param mixed $createdAt
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
	}
}