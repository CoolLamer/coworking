<?php
namespace Remoteo\Contact;

use Kdyby\Doctrine\EntityManager;
use Nette\Http\Request;
use Nette\Security\Identity;
use Nette\Utils\DateTime;
use Remoteo\Services\BaseService;
use Remoteo\Services\FacebookService;

class ContactService extends BaseService
{
	/**
	 * @var Request
	 */
	private $request;
	/**
	 * @var FacebookService
	 */
	private $facebookService;

	public function __construct(EntityManager $entityManager, Request $request, FacebookService $facebookService)
	{
		parent::__construct($entityManager);
		$this->request = $request;
		$this->facebookService = $facebookService;
	}

	public function getAdminTableQuery()
	{
		return $this->entityManager->createQueryBuilder()
			->select('c')
			->from(Contact::getClassName(), 'c');
	}

	public function createContact($email)
	{
		$contact = new Contact();
		$contact->setEmail($email);
		$contact->setIpAddress($this->request->getRemoteAddress());
		$contact->setUserAgent($this->request->getHeader('User-Agent'));
		$contact->setCreatedAt(new DateTime());
		$this->entityManager->persist($contact);
		$this->entityManager->flush();
		return $this->createContactIdentity($contact);
	}

	private function createContactIdentity(Contact $contact)
	{
		return new Identity($contact->getId(), ['contact']);
	}

	public function createContactFromFacebook($accessToken)
	{
		$data = $this->facebookService->loadContactData($accessToken);
		$contact = new Contact();
		$contact->setEmail($data['email']);
		$contact->setIpAddress($this->request->getRemoteAddress());
		$contact->setUserAgent($this->request->getHeader('User-Agent'));
		$contact->setCreatedAt(new DateTime());
		$contact->setFacebookToken($accessToken);
		$this->entityManager->persist($contact);
		$this->entityManager->flush();
		return $this->createContactIdentity($contact);
	}

	/**
	 * @param $id
	 * @return null|Contact
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 * @throws \Doctrine\ORM\TransactionRequiredException
	 */
	public function get($id)
	{
		return $this->entityManager->find(Contact::getClassName(), $id);
	}
}