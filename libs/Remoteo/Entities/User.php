<?php
namespace Remoteo\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette\Security\Passwords;

/** @ORM\Entity */
class User extends BaseEntity
{
	const LEVEL_ADMIN = 'admin';
	const LEVEL_USER = 'user';

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $userName;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $level;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $password;

	public function setPassword($password)
	{
		$this->password = Passwords::hash($password);
	}
} 