<?php
namespace Remoteo\Entities;


use Tracy\Debugger;
use Nette\Reflection\Property;

class BaseEntity extends \Kdyby\Doctrine\Entities\BaseEntity{
	public function toArray()
	{
		$resultArray = [];

		if ($this->getReflection()->implementsInterface('Doctrine\ORM\Proxy\Proxy')) {
			$reflectionClass = $this->getReflection()->getParentClass();
		} else {
			$reflectionClass = $this->getReflection();
		}

		foreach ($reflectionClass->getProperties() as $property) {
			$property->setAccessible(TRUE);
			$value = $property->getValue($this);
			$property->setAccessible(FALSE);

			if ($property->getAnnotation('ORM\Column')) {
				$resultArray[$property->name] = $value;
			} elseif (
				is_object($value)
				&& method_exists($value, 'getReflection')
				&& method_exists($value, 'getId')
			) {
				/** @var Property $value */
				$namespace = $value->getReflection()->getNamespaceName();
				if ($value->getReflection()->implementsInterface('Doctrine\ORM\Proxy\Proxy') || $namespace == 'Dispatchingo\Entities') {
					// Get value from OneToOne or ManyToOne relations
					// Sometimes the column somethingId is not defined due to declared object Something
					$resultArray[$this->createRelationIdName(get_class($value))] = $value->getId();
				}
			} else { //If is property empty load class from annotation and create property name
				$possibleAnnotations = [
					'ORM\ManyToOne',
					'ORM\OneToOne',
				];

				$annotations = $property->getAnnotations();
				foreach ($possibleAnnotations as $possibleAnnotation) {
					$resultArray = $this->checkAnnotation($annotations, $possibleAnnotation, $resultArray);
				}
			}
		}

		return $resultArray;
	}

	/**
	 * @param string $className
	 * @return string
	 */
	public static function createRelationIdName($className)
	{
		$parts = explode('\\', $className);
		if ($parts > 1) {
			$className = array_pop($parts);
		}

		return lcfirst($className) . 'Id';
	}

	/**
	 * @param $annotations
	 * @param $possibleAnnotation
	 * @param $resultArray
	 * @return mixed
	 */
	private function checkAnnotation($annotations, $possibleAnnotation, $resultArray)
	{
		foreach ($annotations as $key => $annotation) {
			if (strpos(mb_strtolower($possibleAnnotation), mb_strtolower($key)) !== FALSE) {
				$annotationData = end($annotation);
				$resultArray[$this->createRelationIdName($annotationData['targetEntity'])] = NULL;
			}
		}
		return $resultArray;
	}

	public function fromArray($inputArray, $ignoredValues = [], $mapOnly = [])
	{
		if (empty($mapOnly)) {
			$array = $inputArray;
		} else {
			$array = [];
			foreach ($mapOnly as $inputArrayKey) {
				$array[$inputArrayKey] = $inputArray[$inputArrayKey];
			}
		}

		foreach ($array as $key => $value) {
			if (in_array($key, $ignoredValues)) {
				continue;
			}

			$method = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
			if (method_exists($this, $method)) {
				$this->$method($value);
			} else {
				Debugger::barDump("Method '$method' do not exist on entity{$this::getClassName()}");
			}
		}
	}

	/**
	 * @param mixed $value
	 * @return float
	 */
	protected function convertToFloat($value)
	{
		$value = str_replace(',', '.', $value);
		return (float)$value;
	}
}