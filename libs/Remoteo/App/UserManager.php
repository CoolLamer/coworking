<?php
namespace Remoteo\App;

use Kdyby\Doctrine\EntityManager;
use Nette;
use Nette\Security\Passwords;
use Remoteo\Entities\User;

class UserManager extends Nette\Object implements Nette\Security\IAuthenticator
{
	/** @var EntityManager */
	private $entityManager;


	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}


	/**
	 * @param array $credentials
	 * @return Nette\Security\Identity|Nette\Security\IIdentity
	 * @throws \Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;
		/** @var User $user */
		$user = $this->entityManager->getRepository(User::getClassName())->findOneByUserName($username);

		if (!$user) {
			throw new Nette\Security\AuthenticationException('messages.user.user-not-exist', self::IDENTITY_NOT_FOUND);
		} elseif (!Passwords::verify($password, $user->password)) {
			throw new Nette\Security\AuthenticationException('messages.user.invalid-password', self::INVALID_CREDENTIAL);

		} elseif (Passwords::needsRehash($user->password)) {
			$user->hash = Passwords::hash($password);
		}

		$arr = $user->toArray();
		unset($arr['password']);
		return new Nette\Security\Identity($user->id, $user->level, $arr);
	}


	/**
	 * @param $username
	 * @param $password
	 * @param bool $admin
	 */
	public function add($username, $password, $admin = false)
	{
		$user = new User();
		$user->userName = $username;
		if ($admin) {
			$user->level = User::LEVEL_ADMIN;
		} else {
			$user->level = User::LEVEL_USER;
		}

		$user->password = $password;
		$this->entityManager->persist($user);
		$this->entityManager->flush();
	}

}
