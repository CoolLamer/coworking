<?php
namespace Remoteo\Commands;

use Remoteo\App\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddUserCommand extends Command
{
    protected function configure()
    {
        $this->setName('app:add-admin')
            ->setDescription('Add admin to system')
            ->addArgument('username', InputArgument::REQUIRED)
            ->addArgument('password', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var UserManager $userManager */
        $userManager = $this->getHelper('container')->getByType('Remoteo\App\UserManager');
        $userManager->add($input->getArgument('username'), $input->getArgument('password'), TRUE);
    }
}